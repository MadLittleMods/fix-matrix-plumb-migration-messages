const assert = require('assert');
const fs = require('fs');
const fetch = require('node-fetch');
const crypto = require('crypto');

const secrets = require('./secrets.json');
assert(secrets.accessToken);

//const BASE_URL = 'http://localhost:18008';
const BASE_URL = 'https://gitter.ems.host';

//const affectedRooms = require('./affected-rooms.json');
const affectedRooms = require('./affected-rooms-failed1.json');
//const affectedRooms = ['!PwmnaVNBSVRNXQksmU:my.matrix.host'];
//const affectedRooms = ['!EdAYmkORUYBdkhdigw:gitter.im', '!TmmnkrbIRwKtFPGxee:matrix.org'];

async function fetchNoticeEventsForRoom(matrixRoomId) {
  // https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-rooms-roomid-messages
  const filter = {
    types: ['m.room.message'],
    senders: [
      '@gitter-badger:gitter.im',
      // For local testing
      '@root:my.matrix.host',
    ],
  };
  const messageEndpoint = `${BASE_URL}/_matrix/client/r0/rooms/${matrixRoomId}/messages?dir=b&limit=100&filter=${encodeURIComponent(
    JSON.stringify(filter)
  )}`;

  const res = await fetch(messageEndpoint, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${secrets.accessToken}`,
      'Content-Type': 'application/json',
    },
  });
  const data = await res.json();
  if (res.status !== 200) {
    console.log(`Non-200 response (got ${res.status})`, data);
  }
  //console.log('data', data, data.chunk.length);

  let events = [];
  if (data.chunk) {
    events = data.chunk;
  }

  return events.filter((event) => {
    if (
      event.content &&
      event.content.msgtype === 'm.notice' &&
      event.content.body &&
      event.content.body.startsWith('This room is being')
    ) {
      return true;
    }

    return false;
  });
}

async function redactEvent(matrixRoomId, eventId) {
  const transactionId = crypto.randomBytes(20).toString('hex');
  const redactionEndpoint = `${BASE_URL}/_matrix/client/r0/rooms/${matrixRoomId}/redact/${eventId}/${transactionId}`;

  const res = await fetch(redactionEndpoint, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${secrets.accessToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      reason: 'Notice posted in wrong room',
    }),
  });
  const data = await res.json();
  console.log('Redacted event', redactionEndpoint, data);
  return data;
}

async function updateNoticeEvent(matrixRoomId, existingNoticeEvent, targetMigrateRoomId) {
  const newMatrixEvent = {
    body: `This room is being migrated, you can use [\`/join ${targetMigrateRoomId} matrix.org\`](https://matrix.to/#/${targetMigrateRoomId}?via=matrix.org) to get to the new room`,
    formatted_body: `This room is being migrated, you can use <a href="https://matrix.to/#/${targetMigrateRoomId}?via=matrix.org"><code>/join ${targetMigrateRoomId} matrix.org</code></a> to get to the new room`,
    format: 'org.matrix.custom.html',
    msgtype: 'm.notice',
  };

  const matrixContent = {
    ...newMatrixEvent,
    'm.new_content': newMatrixEvent,
    'm.relates_to': {
      event_id: existingNoticeEvent.event_id,
      rel_type: 'm.replace',
    },
  };

  const transactionId = crypto.randomBytes(20).toString('hex');
  const sendEndpoint = `${BASE_URL}/_matrix/client/r0/rooms/${matrixRoomId}/send/m.room.message/${transactionId}`;

  const res = await fetch(sendEndpoint, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${secrets.accessToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(matrixContent),
  });
  const data = await res.json();
}

async function fixNoticeEvent(matrixRoomId, noticeEvent) {
  console.log(
    `Fixing noticeEvent in room ${matrixRoomId}`,
    noticeEvent.event_id,
    noticeEvent.content.body
  );

  let targetMigrateRoomId;
  const matches = noticeEvent.content.body.match(/\/join (.*?) matrix\.org/);
  if (matches) {
    targetMigrateRoomId = matches[1];
  }
  if (!targetMigrateRoomId) {
    throw new Error(
      'Unable to find targetMigrateRoomId in existing notice event',
      noticeEvent.content.body
    );
  }
  console.log('targetMigrateRoomId', targetMigrateRoomId);

  // If we accidentally posted the migration message in the room we're moving to
  // redact the message
  if (matrixRoomId === targetMigrateRoomId) {
    console.log(`  -> Redacting event ${noticeEvent.event_id} in Matrix room ${matrixRoomId}`);
    await redactEvent(matrixRoomId, noticeEvent.event_id);
    return;
  }

  console.log(`  -> Updating notice event ${noticeEvent.event_id} in Matrix room ${matrixRoomId}`);
  await updateNoticeEvent(matrixRoomId, noticeEvent, targetMigrateRoomId);
}

async function fixNoticesInRoom(matrixRoomId) {
  console.log('fixNoticeInRoom', matrixRoomId);
  const noticeEvents = await fetchNoticeEventsForRoom(matrixRoomId);
  if (!noticeEvents || noticeEvents.length === 0) {
    throw new Error(`Unable to find notice event for ${matrixRoomId}`);
  }
  console.log('  -> noticeEvents', noticeEvents.length);

  for await (const noticeEvent of noticeEvents) {
    await fixNoticeEvent(matrixRoomId, noticeEvent);
  }
}

const FAILED_LOG_PATH = '/tmp/failed-migrated-plumbed-matrix-rooms.db';

(async () => {
  try {
    fs.writeFileSync(FAILED_LOG_PATH, '');
  } catch (err) {
    console.log('Failed to create the log file for failed rooms');
    throw err;
  }

  for await (const affectedRoom of affectedRooms) {
    try {
      await fixNoticesInRoom(affectedRoom);

      // Put a delay between each time we process a room
      // to avoid overwhelming and hitting the rate-limits on the Matrix homeserver
      await new Promise((resolve) => {
        setTimeout(resolve, 2000);
      });
    } catch (err) {
      console.log(`Error while fixing room (${affectedRoom})`, err, err.stack);
      fs.appendFileSync(FAILED_LOG_PATH, affectedRoom + '\n');
    }
  }

  console.log('Done fixing all rooms');
})();
